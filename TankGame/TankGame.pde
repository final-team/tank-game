//test commit 10:44pm 11/16/2018
//Tank Location
float tankX=200;
float tankY=350;
float speed=4;

//Banana Location
float bananaX=200;

//Movement
boolean goLeft = false;
boolean goRight = true;
boolean goUp = false;
boolean goDown = false;

boolean moveLeft = false;
boolean moveRight = false;
boolean moveUp = false;
boolean moveDown = false;

//Bounding
int boundaryX1=0;
int boundaryX2=400;
int boundaryY1=0;
int boundaryY2=400;

//Game Over
boolean destroyed = false;

//Tank Colour
float col1= 39;
float col2= 134;
float col3= 19;
float noseColour= 170;

//Background Colour
float backCol1 = 170;
float backCol2 = 136;
float backCol3 = 84;
 
//Point Score
int points;

//Bullet Numbers (Array Size)
int bulletLimit= 15;

//Banana Blessing
boolean bananaBlessed= false;

Tank player = new Tank();

//BULLET ARRAY
Bullets[] bullets = new Bullets[bulletLimit];



void setup() {
  size(400, 400); 
  rectMode(CENTER);
  noStroke();
  initiateBullets();
  println("You're in charge of that tank, soldier! Dodge as much enemy fire as possible!");
}

void initiateBullets() {
  for (int i= 0; i < bullets.length; i++) {
    //Value of 400 for locationY is to create slight varience in wave of bullets
    bullets[i]= new Bullets(random(0, 400), -200, random(-1, 1.5), random(2, 7));
  }
}

void draw() {

  background(backCol1, backCol2, backCol3);

  player.sprite();
  player.update();

  achievements();
  display();

  for (int i= 0; i < bullets.length; i++) {
    bullets[i].update();
  }
  for (int i= 0; i < bullets.length; i++) {
    bullets[i].display();
    bullets[i].collision();
  }
}



//MOVEMENT CONTROLS/SWITCHES
void keyPressed() {

  //These pressed key values begin the movement of the tank's values

  if (key == 'a') {
    moveLeft = true;
    goLeft = true;
    goRight = false;
    goUp = false;
    goDown = false;
  }

  if (key == 'd') {
    moveRight = true;
    goRight = true;
    goLeft = false;
    goUp = false;
    goDown = false;
  }

  if (key == 'w') {
    goUp = true;
    moveUp = true;
    goLeft = false;
    goRight = false;
    goDown = false;
  }

  if (key == 's') {
    goDown = true;
    moveDown = true;
    goLeft = false;
    goRight = false;
    goUp = false;
  }

  if (destroyed == true) {
    if (key == 'r' ) {
      points = 0;
      bananaBlessed = false;
      noseColour = 175;
      destroyed = false;
    }
  }
}



//MOVEMENT CONTROL/SWITCH RESET
void keyReleased() {

  //These released keys stop movement by changing boolean values to false

  if (key == 'a') {
    moveLeft = false;
  }

  if (key == 'd') {
    moveRight = false;
  }

  if (key == 'w') {
    moveUp = false;
  }

  if (key == 's') {
    moveDown = false;
  }
}
