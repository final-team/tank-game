boolean ach1 = false;
boolean ach2 = false;
boolean ach3 = false;
boolean ach4 = false;
boolean ach5 = false;

void display() {
  fill(7, 245, 223);
  textSize(14);
  text("Score: " +points, 330, 385); 
  
  if (destroyed == false) {
    
    fill(255);
    textSize(15);
    text("Dodge the shots!",0,395);
  }

  if (destroyed == true) {

    fill(255, 60, 15);
    textSize(26);
    text("GAME OVER", 125, 200);
    fill(255);
    textSize(20);
    text("Press 'R' to Revive", 115, 250);
  }
}

void achievements() {

  if (points == 70 && ach1 == false) {

    println("ACHIEVEMENT Learning the Ropes: Get a score of 70");
    col1 = 183;
    col2 = 9;
    col3 = 9;
    ach1 = true;
  }
  
  if (points == 100 && ach5 == false) {

    println("ACHIEVEMENT Getting Better: Get a score of 100");
    col1 = 242;
    col2 = 150;
    col3 = 0;
    ach5 = true;
  }

  if (points == 150 && ach2 == false) {

    println("ACHIEVEMENT Playing with Fire: Get a score of 150"); 
    col1 = 64;
    col2 = 167;
    col3 = 149;
    ach2 = true;
  }
  if (points == 200 && ach3 == false) {

    println("ACHIEVEMENT Battlefield Veteran: Get a score of 200");
    col1 = 120;
    col2 = 120;
    col3 = 120;
    ach3 = true;
  }
  if (points == 235 && ach4 == false) {

    println("ACHIEVEMENT Dodging Master: Get a score of 235");
    col1 = 212;
    col2 = 216;
    col3 = 11;
    ach4 = true;
  }
  if (points >= 270) {

    println("ACHIEVEMENT Neo From the Matrix: Get a score of 270 and become Neo");
    col1 = random(0, 255);
    col2 = random(0, 255);
    col3 = random(0, 255);
  }
  if (points >= 300) {

    println("ACHIEVEMENT The World's Changing O_O: Get a score of 300");
    backCol1 = random(0,255);
    backCol2 = random(0,10);
  }
  if (tankX <= bananaX+5 && tankX >= bananaX-5 && tankY >= -30+(frameCount % 750)*2-5) {
    if (tankY <= -30+(frameCount % 750)*2 + 5) {
      if (bananaBlessed==false) {
        println("You have recieved the mystical BANANA BLESSING");
        noseColour=240;
        points = points +15;
        bananaBlessed = true;
      }
    }
  }
}
