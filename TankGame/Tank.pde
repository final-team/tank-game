class Tank {

  void sprite() {
	
	//Debris
	fill(129, 74, 14, 200);
    rect(35, (frameCount % 80)*10, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(50, (frameCount % 200)*2, sin(frameCount/2), 2+sin(frameCount/2));
    rect(80, (frameCount % 120)*4, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(150, (frameCount % 95)*6, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(260, (frameCount % 64)*8, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(180, (frameCount % 77)*7, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(320, (frameCount % 116)*12, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(350, (frameCount % 200)*2.5, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(225, (frameCount % 154)*3, 2+sin(frameCount/2), 2+sin(frameCount/2));
    rect(145, (frameCount % 99)*5, 2+sin(frameCount/2), 2+sin(frameCount/2));

    //Banana Body
    fill(247, 255, 36);
    ellipse(bananaX, -30+(frameCount % 750)*2, 30, 30);

    //Sphere that blends with background
    fill(backCol1, backCol2, backCol3);
    ellipse(bananaX+10, -30+(frameCount % 750)*2, 30, 30);

    //Banana Ends
    fill(50, 50, 50);
    ellipse(bananaX+6, -45+(frameCount % 750)*2, 5, 4);
    ellipse(bananaX+6, -15+(frameCount % 750)*2, 5, 4);
    
    //CREATES RIGHT MOVING SPRITE
    if (goRight == true && goLeft ==false && destroyed == false) {

      //CANNON
      fill(noseColour);
      rect(tankX+20, tankY-10-sin(frameCount/2), 25, 5);

      //UPPER PORTION
      fill(col1+25, col2-20, col3-3);
      ellipse(tankX, tankY-8-sin(frameCount/2), 20, 20);

      //CENTRAL BODY
      fill(col1, col2, col3);
      rect(tankX, tankY-sin(frameCount/2), 30, 15);

      //WHEEL (OUTER)
      fill(133);
      ellipse(tankX, tankY+5, 40, 15);

      //WHEEL (INNER)
      fill(180);
      ellipse(tankX, tankY+5, 35, 10);
    }

    //CREATES UPWARD MOVING SPRITE
    if (goUp == true && goDown == false && destroyed == false) {

      //UPPER PORTION
      fill(col1+25, col2-20, col3-3);
      ellipse(tankX, tankY-8-sin(frameCount/2), 20, 20);

      //CENTRAL BODY
      fill(col1, col2, col3);
      rect(tankX, tankY-sin(frameCount/2), 25, 15);

      //WHEEL
      fill(133);
      ellipse(tankX-12.5, tankY+5, 10, 17);
      ellipse(tankX+12.5, tankY+5, 10, 17);
    }

    //CREATES LEFT MOVING SPRITE
    if (goLeft == true && goRight == false && destroyed == false) {

      //CANNON
      fill(noseColour);
      rect(tankX-20, tankY-10-sin(frameCount/2), 25, 5);

      //UPPER PORTION
      fill(col1+25, col2-20, col3-3);
      ellipse(tankX, tankY-8-sin(frameCount/2), 20, 20);

      //CENTRAL BODY
      fill(col1, col2, col3);
      rect(tankX, tankY-sin(frameCount/2), 30, 15);

      //WHEEL (OUTER)
      fill(133);
      ellipse(tankX, tankY+5, 40, 15);

      //WHEEL (INNER)
      fill(180);
      ellipse(tankX, tankY+5, 35, 10);
    }
    //CREATES DOWNWARD MOVING SPRITE
    if (goDown == true && goUp == false && destroyed == false) {

      //UPPER PORTION
      fill(col1+25, col2-20, col3-3);
      ellipse(tankX, tankY-8-sin(frameCount/2), 20, 20);

      //CENTRAL BODY
      fill(col1, col2, col3);
      rect(tankX, tankY-sin(frameCount/2), 25, 15);

      //WHEEL
      fill(133);
      ellipse(tankX-12.5, tankY+5, 10, 17);
      ellipse(tankX+12.5, tankY+5, 10, 17);

      //CANON
      fill(noseColour);
      rect(tankX, tankY-sin(frameCount/2), 6, 20);
    }

    //CREATE DESTROYED TANK
    if (key=='e') {
      destroyed = true;
    }

    if (destroyed == true) {

      //DESTROYED BODY
      fill(col1+11, col2-60, col3-25);
      rect(tankX, tankY, 25, 15);

      //DESTROYED WHEEL
      fill(100);
      ellipse(tankX-12.5, tankY+5, 10, 10);
      ellipse(tankX+12.5, tankY+5, 10, 17);

      //DESTROYED CANNON
      fill(noseColour-70);
      quad(tankX-4.5, tankY-12.5, tankX, tankY-8.5, tankX-18, tankY+12.5, tankX-22.5, tankY+8.5);

      //FLAMES
      fill(162, 5, 5, 150);
      ellipse(tankX-10, tankY-20, 10+sin(frameCount/5), 35+sin(frameCount)*2);
      fill(230, 234, 31, 80);
      ellipse(tankX-10, tankY-10, 5+sin(frameCount/5), 15+sin(frameCount)*2);

      fill(162, 5, 5, 150);
      ellipse(tankX+5, tankY, 8+sin(frameCount/5), 25+sin(frameCount)*2);
      fill(230, 234, 31, 80);
      ellipse(tankX+5, tankY+8, 4+sin(frameCount/5), 10+sin(frameCount)*2);
    }

    //Spotlight #1
    fill(234, 199, 80, 140);
    ellipse(300, -10+(frameCount % 235)*6, random(55, 60), random(55, 60));

    //Spotlight #2
    fill(234, 199, 80, 140);
    ellipse(100, -20+(frameCount % 280)*4, random(55, 60), random(55, 60));
  }

  void update() {

    //Updates tank position to move to the right
    if (moveRight == true && tankX <= boundaryX2 && destroyed == false) {

      tankX= tankX + speed;
    }



    //Updates tank position to move to the left
    if (moveLeft == true && tankX >= boundaryX1 && destroyed == false) {

      tankX= tankX - speed;
    }
    //Updates tank position to move upwards
    if (moveUp == true && tankY >= boundaryY1 && destroyed == false) {

      tankY= tankY - speed;
    }



    //Updates tank position to move downwards
    if (moveDown == true && tankY <= boundaryY2 && destroyed == false) {

      tankY= tankY + speed;
    }
  }
}
