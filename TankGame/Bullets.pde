class Bullets { 

  //Sets initial velocity and location for bullet
  PVector location= new PVector(random(0, 400), random(-100, -400));
  PVector velocity = new PVector(random(-.25, .25), random(.25, 2.5));



  //Constructor
  Bullets(float x, float y, float velX, float velY) {
    location.x=x;
    location.y=y;
    velocity.x=velX;
    velocity.y=velY;
  }

  void display() {
    fill(246, 250, 58);
    ellipse(location.x, location.y, 2, 15);
  }

  void update() {
    if (location.y <= boundaryY2+50) {
      location.add(velocity);
    }
    if (location.y > boundaryY2+50) {
      if (destroyed == false) {
        points = points + 1;
      }
      location.y = -50;
      location.x = random(0, 400);
      velocity.x = random(-1, 1);
    }
  }

  void collision() {

    if (dist(location.x, location.y, tankX, tankY)<=15) {
      if (destroyed == false) {
        location.x = random(0, 400);
        location.y = -50;
        velocity.x = random(-1, 1);
        destroyed = true;
        println("Tank Down! Tank Down! (Refresh page to try again)");
      }
    }
  }
}
